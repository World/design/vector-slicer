# Vector Slicer

Export multi-page PDFs from an SVG

## Why?

Inkscape is the only semi-decent visual editor we have, so in many cases you want to use it for multi-page things such as presentaiton Slides. Unfortunately Inkscape doesn't have artboards/multi-page export though, which makes e.g. exporting a slide deck as PDF a pain.

This app is a somewhat crude but convenient way to do that kind of multi-page export, to avoid hacky scripts or manual page by page exporting.

## How it works

When opening an SVG, the app looks for a group with the ID `pages`. Every rectangle in this group will be a separate page in the exported PDF. These pages will contain of everything that is visible within the boundaries of that rectangle on the canvas. The behavior is similar to that of Artboards in Sketch, in case you're familiar with that.

The sorting order of pages in the PDF depends on their vertical position on the page, you can just place them all above one another on the canvas in the order you want.

Use [this template](https://gitlab.gnome.org/World/design/vector-slicer/blob/master/data/template.svg) to get started.


## Credits

Based on Bilal Elmoussaoui's [GTK Rust Template](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template).

