use gtk::prelude::*;

use crate::config::{APP_ID, PROFILE};
use crate::window_state;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    appmenu_button: gtk::MenuButton,
    pub dest_button: gtk::FileChooserButton,
    pub progress_bar: gtk::ProgressBar,
    pub ready_button: gtk::Button,
    pub ready_label: gtk::Label,
}

impl Window {
    pub fn new() -> Self {
        let settings = gio::Settings::new(APP_ID);
        let builder = gtk::Builder::new_from_resource("/org/gnome/design/VectorSlicer/window.ui");
        let window_widget: gtk::ApplicationWindow = builder.get_object("window").unwrap();
        let appmenu_btn: gtk::MenuButton = builder.get_object("appmenu_button").unwrap();
        let dest_button: gtk::FileChooserButton = builder.get_object("dest_button").unwrap();
        let progress_bar: gtk::ProgressBar = builder.get_object("progress_bar").unwrap();
        let ready_label: gtk::Label = builder.get_object("ready_label").unwrap();
        let ready_button: gtk::Button = builder.get_object("ready_button").unwrap();

        // Set default folder for export button
        if let Some(file) = glib::get_user_special_dir(glib::UserDirectory::Documents) {
            dest_button.set_current_folder(file);
        }

        if PROFILE == "Devel" {
            window_widget.get_style_context().add_class("devel");
        }

        let window = Window {
            widget: window_widget,
            appmenu_button: appmenu_btn,
            dest_button,
            progress_bar,
            ready_label,
            ready_button,
        };

        window.init(settings);
        window
    }

    pub fn init(&self, settings: gio::Settings) {
        // setup app menu
        let menu_builder = gtk::Builder::new_from_resource("/org/gnome/design/VectorSlicer/menu.ui");
        let popover_menu: gtk::PopoverMenu = menu_builder.get_object("popover_menu").unwrap();
        self.appmenu_button.set_popover(Some(&popover_menu));
        // load latest window state
        window_state::load(&self.widget, &settings);

        // save window state on delete event
        self.widget.connect_delete_event(move |window, _| {
            window_state::save(&window, &settings);
            Inhibit(false)
        });
    }
}
